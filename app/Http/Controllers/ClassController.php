<?php

namespace App\Http\Controllers;

use App\Models\SchoolClass;
use Illuminate\Http\Request;
use Carbon\Carbon;
class ClassController extends Controller
{
    //crruent time to future scheduled classes
    public function ScheduleClasses(Request $request)
    {
       
        try {
            // $studentid = Auth::user()->id;
            $studentid= $request->student_id;
            $teacherid= $request->teacher_id;
      
            $reschedule_classes = SchoolClass::where('student_id', $studentid)->where('teacher_id',$teacherid)->whereDate('start_time', '>=', Carbon::now()->format('Y-m-d'))
            ->orderBy('start_time','asc')->get();
    
             foreach ($reschedule_classes as $classes)
              {
               $available_classes_to_schedule[] = $classes;
              }
                   return response()->json(['status' => true,
                   'message' => 'success',
                    'payload' => $available_classes_to_schedule]);
                 } 
   
           catch (\Exception $exception) {
           return response()->json([
               'status' => false,
               'message' => 'not available scheduled classes',
               'payload' => []
           ]);
       } 
    }

       
       
     //free slots of same teacher to makeup the class for requested student 
    public function AvailableSlots(Request $request)
    {
      
    try
                {
                    $teacherid = $request->teacher_id;
                    $studentid = $request->student_id;
                    $bool=true;
                    $available_slots2[]=(array) null;
                    $student_class = SchoolClass::where('teacher_id', $teacherid)->where('student_id',$studentid)->whereDate('start_time', '>=', Carbon::now()->format('Y-m-d'))->where('class_status',NULL)
                    ->orderBy('start_time','asc')->get();
                    $available_slots=SchoolClass::where('teacher_id', $teacherid)->whereDate('start_time', '>=', Carbon::now())->where('class_status',0)
                    ->orderBy('start_time','asc')->get();
                  
                    foreach($available_slots as $i)
                    {  
                
                       foreach($student_class as $j)
                        { 
                           
                            if(Carbon::parse($i->start_time) ==  Carbon::parse($j->start_time))
                            {
                               $bool=false;
                           
                              }
                            } //inner
                            $starti  = Carbon::parse($i->start_time)->format('i');
                            $startj   = Carbon::parse($j->start_time)->format('i');
                            $endi  = Carbon::parse($i->end_time)->format('i');
                            $endj  = Carbon::parse($j->end_time)->format('i');// every class has same duration as free slots duration
    
                       
                         if($bool==true && (($starti-$startj)==0 || ($startj-$starti)==0) )// every class has same duration as free slots duration
                         { 
                            $available_slots2[]=$i;
                         }
                         else
                         {
                           $bool=true;
                         }
                           
                        } //outer
                   
                
                        return response()->json(['status'=>true,
                       'message'=>'available slots ',
                       'payloads'=> $available_slots2]);
                    }

                   catch(\Exception $exception)
                   {
                    return response()->json(['status'=>false,
                    'message'=>'failed',
                    'payloads'=> $exception->getMessage()]);  
              }
            }

       
     
     
       
     
     



    
}
