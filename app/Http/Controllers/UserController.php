<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
class UserController extends Controller
{
    public function StudentStore(Request $request)
    {
        try{
       $data= request()->validate([

         'name'=>'required',
         'email'=>'required|email',
         'password'=>'required',
         'type_id'=>'required'
          

        ]);
        $data['password'] = Hash::make($request->password);
        $student=new User();
        $chk=$student->create($data);
        $studentid=$chk->id;
        if($chk)
        {
          
          $this->UpdateFee($studentid);
        }
        
        return response()->json(['status'=>true,
        'message'=>'success ',
        'payloads'=> $chk]);
    }
     catch(\Exception $e)
     {
        return response()->json(['status'=>false,
        'message'=>'failed',
        'payloads'=> $e->getMessage()]);  
     }

    }
    
    private function UpdateFee($studentid)
    {
        $data=request()->validate(['class_fee'=>'required','class_type'=>'required']);
        $fee=$data['class_fee'];
        $c_type=$data['class_type'];
       
        DB::table('student_fee')->insert(
            array('student_id' =>$studentid,'class_fee'=> $fee,'class_type'=>$c_type)
        );
    }
}
