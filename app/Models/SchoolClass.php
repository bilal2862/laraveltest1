<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SchoolClass extends Model
{
    use HasFactory;

    public function teacher()
    {
        return $this->belongsTo(User::class, 'teacher_id', 'id')->first();
    }

    public function student()
    {
        return $this->belongsTo(User::class, 'student_id', 'id')->first();
    }
}
