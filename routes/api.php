<?php

use App\Http\Controllers\ClassController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('schedule_classes',[ClassController::class,'ScheduleClasses']);
Route::get('availableslots_tomakeup',[ClassController::class,'AvailableSlots']);

Route::post('student_store',[UserController::class,'StudentStore']);

